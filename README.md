# Data Structures C 

> author : Alexandre Lithaud

- [Data Structures C](#data-structures-c)
- [Array based data structure :](#array-based-data-structure-)
  - [Stack](#stack)
  - [Queue](#queue)
  - [Priority Queue](#priority-queue)
- [Tree based data structure :](#tree-based-data-structure-)
  - [Binary Search Tree (BST) //TODO](#binary-search-tree-bst-todo)
  - [Adelson-Velsky and Landis Tree (AVL) //TODO](#adelson-velsky-and-landis-tree-avl-todo)
  - [2-3-4 Tree //TODO](#2-3-4-tree-todo)
  - [Heap //TODO](#heap-todo)
- [Graph based data structure :](#graph-based-data-structure-)
  - [Graph //TODO](#graph-todo)
- [Instalation](#instalation)

---
# Array based data structure : 

## Stack

- the source [stack.c file](sources/stack.c)
- the header [stack.h file](sources/stack.h)
- the test file [test_stack.c file](test/test_stack.c)

A simple stack that do not have a limit in the number of element you can add to.

The structures used : (see the header file for more information)


> **node structure** :
```
typedef struct n
{
    int value;
    struct n *next;
} node, *p_node;
```

> **stack structure** :
```
typedef struct s
{
    p_node first;
    p_node last;
} stack, *p_stack;
```

The function used : (see the header file for more information)

- **p_stack createStack();**
- **void stackUp(p_stack stack, int value);**
- **int unstack(p_stack stack);**
- **void deleteStack(p_stack stack);**
- **int getSize(p_stack stack);**
- **int isStackEmpty(p_stack stack);**

---
## Queue

- the source [queue.c file](sources/queue.c)
- the header [queue.h file](sources/queue.h)
- the test file [test_queue.c file](test/test_queue.c)

A simple queue that do not have a limit in the number of element you can add to.

The structures used : (see the header file for more information)

> **node structure** :
```
typedef struct n
{
    int value;
    struct n *next;
} node, *p_node;
```

> **queue structure** :
```
typedef struct q
{
    p_node first;
    p_node last;
} queue, *p_queue;
```

The function used : (see the header file for more information)

- **p_queue createQueue();**
- **void queueUp(p_queue queue, int value);**
- **int unQueue(p_queue queue);**
- **void deleteQueue(p_queue queue);**
- **int getSize(p_queue queue);**
- **int isQueueEmpty(p_queue queue);**

---
## Priority Queue

- the source [priority_queue.c file](sources/priority_queue.c)
- the header [priority_queue.h file](sources/priority_queue.h)
- the test file [test_priority_queue.c file](test/test_priority_queue.c)

A simple priority that do not have a limit in the number of element you can add to and is able to change the way its priority opperate.


The structures used : (see the header file for more information)

> **node structure** :
```
typedef struct n
{
    int value;
    int priority;
    struct n *next;
} node, *p_node;
```

> **priority queue structure** :
```
typedef struct p
{
    p_node first;
    int (*comparison)(int, int);
} priority_queue, *p_priority_queue;
```

The function used : (see the header file for more information)

- **p_priority_queue createPriorityQueue(int (\*comparison)(int, int));**
- **void queueUp(p_priority_queue pqueue, int value, int priority);**
- **void unQueue(p_priority_queue pqueue, int \*value, int \*priority);**
- **void deletePriorityQueue(p_priority_queue pqueue);**
- **void printPriorityQueue(p_priority_queue pqueue);**
- **int getSize(p_priority_queue pqueue);**
- **int isPriorityQueueEmpty(p_priority_queue pqueue);**


> We are able to change the comparison of the priority queue :

There are five comparison method written in the file :

- **int comparisonAscending(int x, int y);** ---> Order all the element by there priority : The lower priority first
- **int comparisonDescending(int x, int y);** ---> Order all the element by there priority : The greater priority first
- **int comparisonRand(int x, int y);** ---> Random order to the element
- **int comparisonTrue(int x, int y);** ---> Slower equivalent of a Queue 
- **int comparisonFalse(int x, int y);** ---> Slower equivalent of a Stack 

You can write you own comparison method and use it by using the command `createPriorityQueue(&[Your method])`.

---
# Tree based data structure :

## Binary Search Tree (BST) //TODO

Ex commodo cillum elit do cupidatat sint cupidatat incididunt mollit aliquip. Ipsum ipsum id dolor consectetur ipsum incididunt ex amet proident deserunt deserunt sit. Aliqua eu ut sit fugiat excepteur dolor est nulla.

---
## Adelson-Velsky and Landis Tree (AVL) //TODO

Esse eu excepteur cupidatat ex cillum velit irure ex elit Lorem occaecat. Laboris enim quis ipsum do id occaecat consectetur et ullamco fugiat nulla. Amet elit cillum aliqua cillum.

---
## 2-3-4 Tree //TODO

Cupidatat cupidatat consequat minim magna laboris sint. Anim voluptate duis ipsum eu ex quis sit laborum velit ullamco ipsum exercitation incididunt. Ex officia id enim eiusmod Lorem incididunt voluptate aliqua exercitation ex. Aute nisi sint mollit laborum dolor veniam adipisicing velit nisi mollit veniam. Magna Lorem culpa enim cupidatat deserunt veniam ut pariatur.

---
## Heap //TODO

Consectetur amet ullamco commodo consectetur anim labore ipsum. Eiusmod ullamco proident aliquip occaecat sit cillum. Ea veniam aute nulla eiusmod sunt mollit do nostrud veniam. Incididunt occaecat ad elit quis. Velit ea sit et aliqua tempor dolor cillum nostrud.

---
# Graph based data structure :

## Graph //TODO

Occaecat dolor aliqua cillum laboris. Duis nulla ut est adipisicing sit culpa labore aliquip adipisicing nostrud incididunt. Nulla irure velit quis tempor deserunt. Lorem nisi officia id mollit. Et eiusmod adipisicing veniam nulla voluptate. Ex enim anim ea incididunt dolore cupidatat amet do. Proident qui elit Lorem est sit exercitation.


---
# Instalation

Ut elit incididunt Lorem deserunt labore dolor do laborum. Deserunt laboris reprehenderit magna voluptate consequat excepteur adipisicing eiusmod eiusmod et cupidatat. Reprehenderit culpa excepteur officia dolor cillum. Incididunt proident ea exercitation consequat sunt.