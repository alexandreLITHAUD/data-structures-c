CC = gcc
CFLAGS= -Wall -Werror -g

SRC_LOCATION = sources/
TEST_LOCATION = test/
EXECUTABLES_LOCATION = executables/

.PHONY: clean

######################## BASE : ########################

all: test_stack test_queue test_priority_queue

######################## EXECUTABLES ########################

test_stack: stack.o test_stack.o
	$(CC) $(CFLAGS) -o $(EXECUTABLES_LOCATION)$@ $^

test_queue: queue.o test_queue.o
	$(CC) $(CFLAGS) -o $(EXECUTABLES_LOCATION)$@ $^

test_priority_queue: priority_queue.o test_priority_queue.o
	$(CC) $(CFLAGS) -o $(EXECUTABLES_LOCATION)$@ $^
 
######################## SOURCES .o ########################

stack.o: $(SRC_LOCATION)stack.c $(SRC_LOCATION)stack.h
	$(CC) $(CFLAGS) -c $<

queue.o: $(SRC_LOCATION)queue.c $(SRC_LOCATION)queue.h
	$(CC) $(CFLAGS) -c $<

priority_queue.o: $(SRC_LOCATION)priority_queue.c $(SRC_LOCATION)priority_queue.h
	$(CC) $(CFLAGS) -c $<

######################## TEST .o ########################

test_stack.o: $(TEST_LOCATION)test_stack.c $(SRC_LOCATION)stack.h
	$(CC) $(CFLAGS) -c $<

test_queue.o: $(TEST_LOCATION)test_queue.c $(SRC_LOCATION)queue.h
	$(CC) $(CFLAGS) -c $<

test_priority_queue.o: $(TEST_LOCATION)test_priority_queue.c $(SRC_LOCATION)priority_queue.h
	$(CC) $(CFLAGS) -c $<

######################## CLEAN ######################## 

clean:
	rm -f *.o $(EXECUTABLES_LOCATION)test_stack $(EXECUTABLES_LOCATION)test_queue $(EXECUTABLES_LOCATION)test_priority_queue