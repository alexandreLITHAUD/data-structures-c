#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

p_stack createStack()
{
    return malloc(sizeof(stack)); // Creating a stack
}

void stackUp(p_stack stack, int value)
{
    // creation of the new node
    p_node node = malloc(sizeof(node));
    node->next = NULL;
    node->value = value;

    if (isStackEmpty(stack)) // If there is no node in the stack
    {
        stack->first = node;
        stack->last = node;
    }
    else
    {
        // Take the one at the cuurent end and link it with the new end
        stack->last->next = node;
        // Update the new end
        stack->last = node;
    }
    // return stack;
}

int unstack(p_stack stack)
{
    if (isStackEmpty(stack))
    { // If the stack is empty return -1
        return -1;
    }

    int res;

    if (stack->first == stack->last)
    { // If it has one element return that element

        res = stack->last->value;
        free(stack->last);
        stack->last = NULL;
        stack->first = NULL;

        return res;
    }

    p_node n = stack->first;
    res = stack->last->value;

    while (n->next != stack->last)
    {
        n = n->next;
    }

    n->next = NULL;
    free(stack->last);
    stack->last = n;

    return res;
}

void deleteStack(p_stack stack)
{
    if (stack->first == stack->last)
    { // If stack only have 1 element
        free(stack->first);
    }
    else
    {

        while (!isStackEmpty(stack))
        { // Iterate through all element
            p_node n = stack->first;
            stack->first = stack->first->next;

            free(n);
        }
    }

    free(stack);
}

int getSize(p_stack stack)
{
    if (isStackEmpty(stack))
    { // If stack empty 0
        return 0;
    }

    if (stack->first == stack->last)
    { // If stack has 1 element
        return 1;
    }

    p_node n = stack->first;
    int index = 1;

    while (n != stack->last)
    { // Iterate through the stack
        n = n->next;
        index++;
    }

    return index;
}

int isStackEmpty(p_stack stack)
{
    if (stack->first == NULL && stack->last == NULL)
    { // If stack has no element at all
        return 1;
    }
    return 0;
}
