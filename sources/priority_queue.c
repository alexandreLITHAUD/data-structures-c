#include <stdio.h>
#include <stdlib.h>
#include "priority_queue.h"

/**************** COMPARISON **********************/

int comparisonAscending(int x, int y)
{
    return x < y;
}

int comparisonDescending(int x, int y)
{
    return x > y;
}

int comparisonRand(int x, int y)
{
    return rand() % 1;
}

int comparisonTrue(int x, int y)
{
    return 1;
}

int comparisonFalse(int x, int y)
{
    return 0;
}

/**************** FUNCTIONS **********************/

p_priority_queue createPriorityQueue(int (*comparison)(int, int))
{
    p_priority_queue pqueue = malloc(sizeof(priority_queue));

    pqueue->first = NULL;
    pqueue->comparison = comparison;

    return pqueue;
}

void queueUp(p_priority_queue pqueue, int value, int priority)
{
    // Create the node
    p_node node = malloc(sizeof(node));
    node->priority = priority;
    node->value = value;

    if (isPriorityQueueEmpty(pqueue))
    { // If queue is empty
        pqueue->first = node;
        node->next = NULL;
    }

    else if (getSize(pqueue) == 1)
    {
        if (pqueue->comparison(node->priority, pqueue->first->priority))
        {
            p_node temp = pqueue->first;
            node->next = temp;
            pqueue->first = node;
        }
        else
        {
            pqueue->first->next = node;
            node->next = NULL;
        }
    }

    else
    {

        if (pqueue->comparison(node->priority, pqueue->first->priority))
        {
            p_node temp = pqueue->first;
            node->next = temp;
            pqueue->first = node;
        }
        else
        {

            p_node bef = pqueue->first;
            p_node current = bef->next;

            while (current != NULL && !(pqueue->comparison(node->priority, current->priority)))
            {
                bef = current;
                current = bef->next;
            }

            bef->next = node;
            node->next = current;
        }
    }
}

void unQueue(p_priority_queue pqueue, int *value, int *priority)
{
    if (isPriorityQueueEmpty(pqueue))
    { // If queue is empty
        *value = -1;
        *priority = -1;
    }
    else
    {
        p_node node = pqueue->first;
        *value = node->value;
        *priority = node->priority;
        pqueue->first = node->next;

        free(node);
    }
}

void deletePriorityQueue(p_priority_queue pqueue)
{

    if (!isPriorityQueueEmpty(pqueue))
    {
        while (!isPriorityQueueEmpty(pqueue))
        {
            p_node n = pqueue->first;
            pqueue->first = n->next;

            free(n);
        }
    }

    free(pqueue);
}

void printPriorityQueue(p_priority_queue pqueue)
{

    if (isPriorityQueueEmpty(pqueue))
    {
        printf("Empty pqueue\n");
    }
    else
    {
        p_node node = pqueue->first;
        printf("<---> | ");
        while (node != NULL)
        {
            printf("%i : %i | ", node->value, node->priority);
            node = node->next;
        }
        printf("\n");
    }
}

int getSize(p_priority_queue pqueue)
{
    if (isPriorityQueueEmpty(pqueue))
    {
        return 0;
    }
    else
    {
        int index = 0;
        p_node n = pqueue->first;

        while (n != NULL)
        {
            n = n->next;
            index++;
        }

        return index;
    }
}

int isPriorityQueueEmpty(p_priority_queue pqueue)
{
    return pqueue->first == NULL;
}