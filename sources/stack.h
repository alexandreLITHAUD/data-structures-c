#ifndef STACK_H
#define STACK_H

/******************* STRUCTURES *******************/

// Defining a new type called node,
// which is a struct with an int value and a pointer to another node.
// It is also defining a new type called p_node, which is a pointer to a node.
typedef struct n
{
    int value;
    struct n *next;
} node, *p_node;

// Defining a new type called stack,
// which is a struct with a pointer to a node called first and a pointer to a node called last.
// It is also defining a new type called p_stack, which is a pointer to a stack.
typedef struct s
{
    p_node first;
    p_node last;
} stack, *p_stack;

// TODO
//MAYBE ADD THE BEFORE TO LAST ONE IN ORDER TO HAVE A CONSTANT UN STACK SPEED in O(1) for the moment it is O(n)

/******************* PROTOTYPES *******************/

/**
 * It creates a stack.
 *
 * @return A pointer to a stack.
 */
p_stack createStack();

/**
 * It creates a new node, links it to the end of the stack, and updates the end of the stack
 *
 * @param stack the stack to which we want to add a node
 * @param value the value to be added to the stack
 *
 */
void stackUp(p_stack stack, int value);

/**
 * We traverse the stack until we find the second to last element, then we free the last element and
 * set the second to last element as the last element
 *
 * @param stack The stack to unstack from
 *
 * @return The value of the last element of the stack.
 */
int unstack(p_stack stack);

/**
 * It deletes the stack.
 *
 * @param stack The stack to delete
 */
void deleteStack(p_stack stack);

/**
 * If the stack is empty, return 0. Otherwise, if the first and last nodes are the same, return 1.
 * Otherwise, iterate through the stack until the last node is reached, and return the number of nodes.
 *
 * @param stack The stack to get the size of.
 *
 * @return The size of the stack.
 */
int getSize(p_stack stack);

/**
 * If the stack has no element at all, return 1, else return 0
 *
 * @param stack The stack to check
 *
 * @return the value of the first element in the stack.
 */
int isStackEmpty(p_stack stack);

#endif