#ifndef QUEUE_H
#define QUEUE_H

/******************* STRUCTURES *******************/

// Creating a new type called node,
// which is a struct with an int and a pointer to a node.
// also creating a new type called p_node, which is a pointer to a node.
typedef struct n
{
    int value;
    struct n *next;
} node, *p_node;

// Creating a new type called queue
// which is a struct with two pointers to nodes.
// It is also creating a new type called p_queue, which is a pointer to a queue.
typedef struct q
{
    p_node first;
    p_node last;
} queue, *p_queue;

// TODO
//MAYBE ADD THE BEFORE TO LAST ONE IN ORDER TO HAVE A CONSTANT UN QUEUE SPEED in O(1) for the moment it is O(n)

/******************* PROTOTYPES *******************/

/**
 * It creates a queue.
 *
 * @return A pointer to a queue
 */
p_queue createQueue();

/**
 * It adds a new element to the queue.
 *
 * @param queue The queue to which we want to add a new element.
 * @param value The value to be added to the queue.
 */
void queueUp(p_queue queue, int value);

/**
 * We start at the first node, and keep going until we reach the second to last node. Then we set the
 * second to last node's next pointer to NULL, free the last node, and set the queue's last pointer to
 * the second to last node
 *
 * @param queue a pointer to the queue
 *
 * @return The value of the last node in the queue.
 */
int unQueue(p_queue queue);

/**
 * It deletes the queue.
 *
 * @param queue The queue to be deleted
 */
void deleteQueue(p_queue queue);

/**
 * Iterate through the queue and count the number of nodes.
 *
 * @param queue The queue to get the size of
 *
 * @return The size of the queue
 */
int getSize(p_queue queue);

/**
 * If the queue has no elements, return 1, else return 0
 *
 * @param queue The queue you want to check.
 *
 * @return the value of the first element in the queue.
 */
int isQueueEmpty(p_queue queue);

#endif