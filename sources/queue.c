#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

p_queue createQueue()
{
    return malloc(sizeof(queue)); // Creating a queue
}

void queueUp(p_queue queue, int value)
{

    p_node node = malloc(sizeof(node));
    node->next = NULL;
    node->value = value;

    if (isQueueEmpty(queue)) // If there is no element in the queue
    {
        queue->first = node;
        queue->last = node;
    }
    else
    {
        // Update the new element position
        p_node n = queue->first;
        node->next = n;
        // Update the queue
        queue->first = node;
    }
}

int unQueue(p_queue queue)
{
    if (isQueueEmpty(queue))
    { // If queue is empty we return -1
        return -1;
    }

    int res;

    if (queue->first == queue->last)
    { // If the queue only has one element

        // We remove that element and update the queue
        res = queue->first->value;
        free(queue->last);
        queue->first = NULL;
        queue->last = NULL;

        return res;
    }

    // We search for the before last one in order to update the queue
    p_node n = queue->first;
    res = queue->last->value;

    while (n->next != queue->last)
    {
        n = n->next;
    }

    // We upadte the queue acoordingly
    n->next = NULL;
    free(queue->last);
    queue->last = n;

    return res;
}

void deleteQueue(p_queue queue)
{
    if (queue->first == queue->last)
    { // If queue only have 1 element
        free(queue->first);
    }
    else
    {

        while (!isQueueEmpty(queue))
        { // Iterate through all element
            p_node n = queue->first;
            queue->first = queue->first->next;

            free(n);
        }
    }

    free(queue);
}

int getSize(p_queue queue)
{
    if (isQueueEmpty(queue))
    { // If queue empty 0
        return 0;
    }

    if (queue->first == queue->last)
    { // If queue has 1 element
        return 1;
    }

    p_node n = queue->first;
    int index = 1;

    while (n != queue->last)
    { // Iterate through the queue
        n = n->next;
        index++;
    }

    return index;
}

int isQueueEmpty(p_queue queue)
{
    if (queue->first == NULL && queue->last == NULL)
    { // If queue has no element at all
        return 1;
    }
    return 0;
}