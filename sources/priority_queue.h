#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

/******************* STRUCTURES *******************/

//Creating a struct called node and a pointer to it called p_node.
typedef struct n
{
    int value;
    int priority;
    struct n *next;
} node, *p_node;

//Creating a struct called priority_queue and a pointer to it called p_priority_queue.
typedef struct p
{
    p_node first;
    int (*comparison)(int, int);
} priority_queue, *p_priority_queue;

/******************* PROTOTYPES *******************/

/**
 * It creates a priority queue and returns a pointer to it
 *
 * @param comparison a function that takes two integers and returns an integer.
 *
 * @return A pointer to a priority queue.
 */
p_priority_queue createPriorityQueue(int (*comparison)(int, int));

/**
 * It adds a node to the queue.
 *
 * @param pqueue The priority queue
 * @param value The value of the node
 * @param priority The priority of the node.
 */
void queueUp(p_priority_queue pqueue, int value, int priority);

/**
 * If the queue is empty, set the value and priority to -1, otherwise, set the value and priority to
 * the first node's value and priority, and then free the first node
 * 
 * @param pqueue pointer to the priority queue
 * @param value The value of the node that is being dequeued.
 * @param priority The priority of the element to be inserted.
 */
void unQueue(p_priority_queue pqueue, int *value, int *priority);

/**
 * It deletes the priority queue by deleting all the nodes in the queue and then deleting the queue
 * itself
 * 
 * @param pqueue the priority queue to be deleted
 */
void deletePriorityQueue(p_priority_queue pqueue);

/**
 * It prints the priority queue
 * 
 * @param pqueue pointer to the priority queue
 */
void printPriorityQueue(p_priority_queue pqueue);

/**
 * It returns the number of elements in the priority queue
 * 
 * @param pqueue The priority queue to get the size of.
 * 
 * @return The size of the priority queue.
 */
int getSize(p_priority_queue pqueue);

/**
 * If the first and last pointers are both NULL, then the priority queue is empty.
 *
 * @param pqueue the priority queue to check
 *
 * @return A boolean value.
 */
int isPriorityQueueEmpty(p_priority_queue pqueue);

/******************* COMPARISON *******************/

/**
 * If x is greater than y, return 1, otherwise return 0.
 *
 * @param x The first value to compare.
 * @param y The current element being compared.
 *
 * @return 1
 */
int comparisonAscending(int x, int y);

/**
 * If x is less than y, return 1, otherwise return 0.
 *
 * @param x The first value to compare.
 * @param y The current element being compared.
 *
 * @return The comparison function returns a boolean value.
 */
int comparisonDescending(int x, int y);

/**
 * It returns a random number between 0 and 1
 *
 * @param x the first value to compare
 * @param y the number of elements in the array
 *
 * @return A random number between 0 and 1.
 */
int comparisonRand(int x, int y);

/**
 * It always returns 1
 *
 * @param x The first value to compare.
 * @param y The value to compare to.
 *
 * @return 1
 */
int comparisonTrue(int x, int y);

/**
 * It returns 0
 *
 * @param x The first number to compare.
 * @param y The y coordinate of the pixel to be tested.
 *
 * @return 0
 */
int comparisonFalse(int x, int y);

#endif