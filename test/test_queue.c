#include <stdio.h>
#include <stdlib.h>
#include "../sources/queue.h"

// COLOR
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

#define FALSE 0
#define TRUE 1

void assertCondition(int b, char *str);

int main()
{

    printf(YELLOW "============= QUEUE TEST =============\n" NOCOLOR);

    p_queue queue = createQueue();

    assertCondition(getSize(queue) == 0, "GetSize function 1");

    assertCondition(isQueueEmpty(queue) == TRUE, "isQueueEmpty function 1");

    queueUp(queue, 5);

    assertCondition(getSize(queue) == 1, "GetSize function 2");

    assertCondition(isQueueEmpty(queue) == FALSE, "isQueueEmpty function 2");

    int res = unQueue(queue);

    assertCondition(res == 5, "Queue and unQueue function");

    assertCondition(getSize(queue) == 0, "GetSize function 3");

    assertCondition(isQueueEmpty(queue) == TRUE, "isQueueEmpty function 3");

    // TODO FOR LOOP TEST

    for (int i = 0; i < 20; i++)
    {
        queueUp(queue, i);
    }

    for (int i = 0; i < 20; i++)
    {
        assertCondition(unQueue(queue) == i, "Queue and unQueue function MAJOR");
    }

    //Test the delete of a filled queue
    for(int i = 0; i < 100;i++){
        queueUp(queue,i);
    }

    deleteQueue(queue);

    printf(YELLOW "============= TEST PASSED =============\n" NOCOLOR);

    return EXIT_SUCCESS;
}

/**
 * It takes a boolean as an argument and if it's false, it prints a red message and exits the program.
 * If it's true, it prints a green message
 *
 * @param b The boolean expression to be tested.
 */
void assertCondition(int b, char *str)
{

    if (!b)
    {
        printf(RED "============= TEST FAILED =============\n" NOCOLOR);
        printf(RED "%s \n" NOCOLOR, str);
        exit(EXIT_FAILURE);
    }
    else
    {
        printf(GREEN " >> SUB TEST PASSED\n" NOCOLOR);
        printf(GREEN "%s \n" NOCOLOR, str);
    }
}