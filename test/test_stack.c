#include <stdio.h>
#include <stdlib.h>
#include "../sources/stack.h"

// COLOR
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

#define FALSE 0
#define TRUE 1

void assertCondition(int b, char *str);

int main()
{

    printf(YELLOW "============= STACK TEST =============\n" NOCOLOR);

    p_stack stack = createStack();

    assertCondition(getSize(stack) == 0, "GetSize function 1");

    assertCondition(isStackEmpty(stack) == TRUE, "IsStackEmpty function 1");

    stackUp(stack, 5);

    assertCondition(getSize(stack) == 1, "GetSize function 2");

    assertCondition(isStackEmpty(stack) == FALSE, "IsStackEmpty function 2");

    int res = unstack(stack);

    assertCondition(res == 5, "Stack and Unstack function");

    assertCondition(getSize(stack) == 0, "GetSize function 3");

    assertCondition(isStackEmpty(stack) == TRUE, "IsStackEmpty function 3");

    for (int i = 0; i < 20; i++)
    {
        stackUp(stack, i);
    }

    for (int i = 19; i > 0; i--)
    {
        assertCondition(unstack(stack) == i, "Stack and Unstack function MAJOR");
    }

    // Test the delete of a filled stack
    for(int i = 0; i < 100;i++){
       stackUp(stack, i);
    }

    deleteStack(stack);

    printf(YELLOW "============= TEST PASSED =============\n" NOCOLOR);

    return EXIT_SUCCESS;
}

/**
 * It takes a boolean as an argument and if it's false, it prints a red message and exits the program.
 * If it's true, it prints a green message
 *
 * @param b The boolean expression to be tested.
 */
void assertCondition(int b, char *str)
{

    if (!b)
    {
        printf(RED "============= TEST FAILED =============\n" NOCOLOR);
        printf(RED "%s \n" NOCOLOR, str);
        exit(EXIT_FAILURE);
    }
    else
    {
        printf(GREEN " >> SUB TEST PASSED\n" NOCOLOR);
        printf(GREEN "%s \n" NOCOLOR, str);
    }
}
