#include <stdio.h>
#include <stdlib.h>
#include "../sources/priority_queue.h"

// COLOR
#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

#define FALSE 0
#define TRUE 1

void assertCondition(int b, char *str);

int main()
{
    printf(YELLOW "============= QUEUE TEST =============\n" NOCOLOR);

    /****************** ASCEND *************************/
    p_priority_queue pqueueAcend = createPriorityQueue(&comparisonAscending); // LOWER PRIORITY FIRST

    assertCondition(getSize(pqueueAcend) == 0, "GetSize function 1");
    assertCondition(isPriorityQueueEmpty(pqueueAcend) == TRUE, "isPriorityQueueEmpty function 1 ASCEND");

    queueUp(pqueueAcend, 1, 5);

    assertCondition(getSize(pqueueAcend) == 1, "GetSize function 2");
    assertCondition(isPriorityQueueEmpty(pqueueAcend) == FALSE, "isPriorityQueueEmpty function 2 ASCEND");

    int val, prio;
    unQueue(pqueueAcend, &val, &prio);

    assertCondition(val == 1 && prio == 5, "Queue and unQueue function 1 ASCEND");
    assertCondition(getSize(pqueueAcend) == 0, "GetSize function 4");
    assertCondition(isPriorityQueueEmpty(pqueueAcend) == TRUE, "isPriorityQueueEmpty function 4 ASCEND");

    queueUp(pqueueAcend, 4, 1);
    queueUp(pqueueAcend, 5, 7);
    queueUp(pqueueAcend, 1, 5);
    queueUp(pqueueAcend, 2, 9);
    queueUp(pqueueAcend, 3, 2);
    queueUp(pqueueAcend, 6, 2);

    printPriorityQueue(pqueueAcend);

    assertCondition(getSize(pqueueAcend) == 6, "GetSize function 5");
    assertCondition(isPriorityQueueEmpty(pqueueAcend) == FALSE, "isPriorityQueueEmpty function 5 ASCEND");

    unQueue(pqueueAcend, &val, &prio);
    assertCondition(val == 4 && prio == 1, "Queue and unQueue function MAJOR 1 ASCEND");

    unQueue(pqueueAcend, &val, &prio);
    assertCondition(val == 3 && prio == 2, "Queue and unQueue function MAJOR 2 ASCEND");

    unQueue(pqueueAcend, &val, &prio);
    assertCondition(val == 6 && prio == 2, "Queue and unQueue function MAJOR 3 ASCEND");

    unQueue(pqueueAcend, &val, &prio);
    assertCondition(val == 1 && prio == 5, "Queue and unQueue function MAJOR 4 ASCEND");

    unQueue(pqueueAcend, &val, &prio);
    assertCondition(val == 5 && prio == 7, "Queue and unQueue function MAJOR 5 ASCEND");

    unQueue(pqueueAcend, &val, &prio);
    assertCondition(val == 2 && prio == 9, "Queue and unQueue function MAJOR 6 ASCEND");

    deletePriorityQueue(pqueueAcend);

    /****************** DESCEND *************************/
    p_priority_queue pqueueDescend = createPriorityQueue(&comparisonDescending); // GREATER PRIORITY FIRST

    assertCondition(getSize(pqueueDescend) == 0, "GetSize function 1");
    assertCondition(isPriorityQueueEmpty(pqueueDescend) == TRUE, "isPriorityQueueEmpty function 1 DESCEND");

    queueUp(pqueueDescend, 1, 5);

    assertCondition(getSize(pqueueDescend) == 1, "GetSize function 2");
    assertCondition(isPriorityQueueEmpty(pqueueDescend) == FALSE, "isPriorityQueueEmpty function 2 DESCEND");

    unQueue(pqueueDescend, &val, &prio);

    assertCondition(val == 1 && prio == 5, "Queue and unQueue function 1 DESCEND");
    assertCondition(getSize(pqueueDescend) == 0, "GetSize function 4");
    assertCondition(isPriorityQueueEmpty(pqueueDescend) == TRUE, "isPriorityQueueEmpty function 4 DESCEND");

    queueUp(pqueueDescend, 1, 5);
    queueUp(pqueueDescend, 2, 9);
    queueUp(pqueueDescend, 3, 2);
    queueUp(pqueueDescend, 4, 1);
    queueUp(pqueueDescend, 5, 7);
    queueUp(pqueueDescend, 6, 2);

    printPriorityQueue(pqueueDescend);

    assertCondition(getSize(pqueueDescend) == 6, "GetSize function 5");
    assertCondition(isPriorityQueueEmpty(pqueueDescend) == FALSE, "isPriorityQueueEmpty function 5 DESCEND");

    unQueue(pqueueDescend, &val, &prio);
    assertCondition(val == 2 && prio == 9, "Queue and unQueue function MAJOR 1 DESCEND");

    unQueue(pqueueDescend, &val, &prio);
    assertCondition(val == 5 && prio == 7, "Queue and unQueue function MAJOR 2 DESCEND");

    unQueue(pqueueDescend, &val, &prio);
    assertCondition(val == 1 && prio == 5, "Queue and unQueue function MAJOR 3 DESCEND");

    unQueue(pqueueDescend, &val, &prio);
    assertCondition(val == 3 && prio == 2, "Queue and unQueue function MAJOR 4 DESCEND");

    unQueue(pqueueDescend, &val, &prio);
    assertCondition(val == 6 && prio == 2, "Queue and unQueue function MAJOR 5 DESCEND");

    unQueue(pqueueDescend, &val, &prio);
    assertCondition(val == 4 && prio == 1, "Queue and unQueue function MAJOR 6 DESCEND");

    deletePriorityQueue(pqueueDescend);

    printf(YELLOW "============= TEST PASSED =============\n" NOCOLOR);

    return EXIT_SUCCESS;
}

/**
 * It takes a boolean as an argument and if it's false, it prints a red message and exits the program.
 * If it's true, it prints a green message
 *
 * @param b The boolean expression to be tested.
 */
void assertCondition(int b, char *str)
{

    if (!b)
    {
        printf(RED "============= TEST FAILED =============\n" NOCOLOR);
        printf(RED "%s \n" NOCOLOR, str);
        exit(EXIT_FAILURE);
    }
    else
    {
        printf(GREEN " >> SUB TEST PASSED\n" NOCOLOR);
        printf(GREEN "%s \n" NOCOLOR, str);
    }
}